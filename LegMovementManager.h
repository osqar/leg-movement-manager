#ifndef LegMovementManager_h
#define LegMovementManager_h

#include <BaBauHound.h>
#include <Queu.h>
#include <Arduino.h>
#include <inttypes.h>

#define MaxNumberOfLegs 4
#define AccelerationFactor 4
#define True 1
#define False 0

//Max Velocity
#define MaxVelocity 0.1

//Leg Space Work
//For ElbowDown
#define EDMaxPosX 40
#define EDMinPosX -50
#define EDMaxPosY 165
#define EDMinPosY 120

//Leg Space Work
//For ElbowUp
#define EUMaxPosX 50
#define EUMinPosX -40
#define EUMaxPosY 165
#define EUMinPosY 120

//more like Legs MovementManager
class LegMovementManager
{
	public:	
	void Initialize(); //Initializes
	
	//Service
	void SubscribeLeg(Leg *); //Returns id to subscriber
	void UnSubscribeLeg(int); //doesn't work
	bool isLegSubscribed(int);
	
	uint8_t SubscribedLegs;
	Queu Movements[MaxNumberOfLegs];
	
	//Leg Pointer Array
	Leg * LegBuffer[MaxNumberOfLegs];
	
	//Movement
	void MovementRequest(int,float *);	
	void MovementHandler(int);
	void EnqueuMovement(float, float, float, int,int);
	int DequeuAndRefresh(int); //returns LegID
	long EvaluateAndCodePosition(float,int,int);
	float DecodePosition(long);
	
	uint8_t SubscribedMovements();
	bool FinishedAllMovements();
	
	void Runner();
	
	float MovementFinalX;
	float MovementFinalY;
	float MovementVelocity;
	int MovementElbow;
	bool Moving[MaxNumberOfLegs];
	
	private:
};

//SubscribeLeg -> then subscribe any movements to LEGID

#endif